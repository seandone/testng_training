package com.progleasing;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class SampleTestNGTest
{

	public String testName;

	public SampleTestNGTest(){
		testName = "Simple Test Without Params";
	}

	public SampleTestNGTest(String testName){
		this.testName = testName;
	}

	public void method1(){
	}

	public void method2(){
	}

	public void method3(){
	}

	public void method4(){
	}

	public void method5(){
	}

	public void method6(){
	}

	public void method7(){
	}

	public void method8(){
	}

	public void method9(){
	}

	public void method10(){
	}

}

